Configure, build and enjoy docker for oidb service.

Old but still alive documentation : http://www.jmmc.fr/twiki/bin/view/Jmmc/Software/OiDb#Internal%20documentation

Two containers coworks to implement the VO service:
- oidb-existdb  ( web application + uploader file hosting + VO service (TAP) )
- oidb-postgres ( postgres with pgsphere extension to store metadata records )

The docker-compose file handles build and run steps:
- just run `docker-compose up` and you are gone!
Two volumes are created if not present to guarantee persistency

Have a look into the .env file for configuration values.


Next command have been used to restore an instance : 

- docker-compose up -d oidb-postgres 
( `pg_dump -F t -h localhost -U oidb  oidb > oidb.dump` )
- docker run -it --rm --volume $PWD/oidb.dump/tmp/oidb.dump --link jmmc-oidb-docker_oidb-postgres_1:db --network oidb jmmc-oidb-docker_oidb-postgres:latest pg_restore -h db -U oidb -d oidb /tmp/oidb.dump

- docker-compose up -d oidb-existdb 
( use existdb's backup procedure )
- docker cp /.../backup-existdb-oidb/ jmmc-oidb-docker_oidb-existdb_1:/backup
- docker exec --volume $PWD/backup-dir:/export/backup oidb-existdb_1 java -jar start.jar client --no-gui --xpath   "system:restore('/export/backup', '', '')"
/ or perform the restore from exide 

and don't forget to :
- change the password : ) docker exec oidb-existdb java -jar start.jar client -q -u admin -P '' -x 'sm:passwd("admin", "123K8S")'

A debug image version has been added so we can exec some 'busybox' commands
