#!/bin/bash
# Generate a versioned conf dir with main conf files from a verbatim given  existdb image.
# Output an error code with LINENO if something wrong occurs.

VERSION=$1

if [ -z "$VERSION" ] 
then
  echo "Please give version number for an existdb/existdb docker image."
  echo "see : https://hub.docker.com/r/existdb/existdb/tags"
  exit ${LINENO}
fi

IMAGE="existdb/existdb:$VERSION"
echo "Trying to use $IMAGE"
EXISTDB_DIR=exist_$VERSION

if [ -d $EXISTDB_DIR ]
then 
  echo "$EXISTDB_DIR directory already present"
  exit ${LINENO}
fi


if ! CONTID=$(docker run -d $IMAGE)
then
  echo "Sorry, a problem occured runing $IMAGE as container"
  exit ${LINENO}
fi

echo "Run and copy /exist from new container $CONTID"
docker cp $CONTID:/exist $EXISTDB_DIR

echo "Kill container"
docker kill $CONTID
docker rm $CONTID

CONFFILES="/etc/conf.xml
/etc/webapp/WEB-INF/controller-config.xml
/etc/webapp/WEB-INF/web.xml
/etc/jetty/jetty.xml"
for f in $CONFFILES ; do FILE=$EXISTDB_DIR/$f ; NEWFILE=${f/\/etc/etc_$VERSION} ; mkdir -p $(dirname $NEWFILE) ; cp $FILE $NEWFILE; done

rm -rf $EXISTDB_DIR

echo "Please find a copy of files to update from $IMAGE in etc_$VERSION"


diff etc etc_$VERSION
